import { createApp } from 'vue'
import App from './App.vue'
import './index.css'
import {routes} from './routes/routes'
import {createRouter, createWebHistory} from 'vue-router'
import Card from './components/Card.vue'

const router = createRouter({
    history: createWebHistory(),
    routes
})
const app = createApp(App);
app.use(router)
app.component('Card', Card)
app.mount('#app')
