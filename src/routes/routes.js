import Notes from "../components/Notes.vue";
import Home from "../components/Home.vue";
import ShowNote from "../components/ShowNote.vue";
import EditNote from "../components/EditNote.vue";

const routes = [
    {
        path: '/', component: Home
    },
    {
        path: '/notes', component: Notes
    },
    {
        path: '/notes/:id',
        component: ShowNote
    },
    {
        path: '/notes/:id/edit',
        component: EditNote
    }
]

export {routes}
