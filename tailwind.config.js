module.exports = {
  content: [
      "./index.html",
      "./src/**/*.{css,vue,js,ts,jsx,tsx}",
      "./src/index.css"
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}
